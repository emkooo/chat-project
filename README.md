# README #

To start, run the main program Chat.java and simply enter http://localhost:4567 into your favourite browser.

### Crypto-chat ###

This is a school project where we are going to create a java based server handling http request. The clients enter the chat through their browser and encrypt their messages which the server forwards to other clients. Only clients with the same pre-shared key will be able to decrypt and read each others messages.