//Establish the WebSocket connection and set up event handlers
var webSocket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/chat/");
webSocket.onmessage = function (msg) {
    updateChat(msg);
};
webSocket.onclose = function () {
    alert("WebSocket connection closed")
};

var passwordused = "";
var username = "Noname";

//Set password if "Set password" is clicked
id("setPass").addEventListener("click", function () {
    passwordused = escapeHtml(id("pass").value);
    document.getElementById('theOldPasswords').innerHTML = '';
});

//Set password if enter is pressed in the input field
id("pass").addEventListener("keypress", function (e) {
    if (e.keyCode === 13) {
        passwordused = escapeHtml(id("pass").value);
        document.getElementById('theOldPasswords').innerHTML = '';      
    }
});

//Set user name if "Set username" is clicked
id("setUsername").addEventListener("click", function () {
    username = escapeHtml(id("username").value);
    document.getElementById('thechoosenusername').innerHTML = '';
    insertLast("thechoosenusername", "<li>" + username + "</li>");
});

//Set user name if enter is pressed in the input field
id("username").addEventListener("keypress", function (e) {
    if (e.keyCode === 13) {
        username = escapeHtml(id("username").value);
        document.getElementById('thechoosenusername').innerHTML = '';
        insertLast("thechoosenusername", "<li>" + username + "</li>");
    }
});

//Send message if "Send" is clicked
id("send").addEventListener("click", function () {
    sendMessage(escapeHtml(id("message").value));
});

//Send message if enter is pressed in the input field
id("message").addEventListener("keypress", function (e) {
    if (e.keyCode === 13) {
        sendMessage(escapeHtml(e.target.value));
    }
});

//Send a message if it's not empty, then clear the input field
function sendMessage(message) {
    if (message !== "") {
        // encrypted message
        var crypt = sjcl.encrypt(passwordused, message);

        if (username == "")
            username = "Noname";
        if (username == "mySecretCombo")
            username = "Noname";

        webSocket.send(username + "mySecretCombo" + crypt);
        id("message").value = "";
    }
}

function getHtml(msg) {
    return "<article> <p>" + msg + "</p> </article>";
}

//Update the chat-panel, and the list of connected users
function updateChat(msg) {
    var data = JSON.parse(msg.data);
    var systemMessage = data.system;
    if (systemMessage === "")
    {
        var decryptedMessage = sjcl.decrypt(passwordused, data.userMessage);
        decryptedMessage = data.time + "- " + data.from + ": " + decryptedMessage;
    } else
    {
        decryptedMessage = data.time + "- " + systemMessage;
    }

    var messageInHtml = getHtml(decryptedMessage);
    insert("chat", messageInHtml);

    id("userlist").innerHTML = "";
    data.userlist.forEach(function (user) {
        insert("userlist", "<li>" + user + "</li>");
    });
}

//Helper function for inserting HTML as the first child of an element
function insert(targetId, message) {
    id(targetId).insertAdjacentHTML("afterbegin", message);
}

function insertLast(targetId, message) {
    id(targetId).insertAdjacentHTML("beforeend", message);
}

//Helper function for selecting element by id
function id(id) {
    return document.getElementById(id);
}

//Helper function to avoid javascript injection by replacing special characters
function escapeHtml(text)
{
    var map =
            {
                '&': '&amp;',
                '<': '',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#039;'
            };

    return text.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}