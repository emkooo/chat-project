import org.eclipse.jetty.websocket.api.*;
import org.json.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static j2html.TagCreator.*;
import static spark.Spark.*;

public class Chat {

    // this map is shared between sessions and threads, so it needs to be thread-safe (http://stackoverflow.com/a/2688817)
    public static Map<Session, String> userUsernameMap = new ConcurrentHashMap<>();
    public static int nextUserNumber = 1; //Assign to username for next connecting user
    

    public static void main(String[] args) {
        staticFiles.location("/public"); //index.html is served at localhost:4567 (default port)
        staticFiles.expireTime(600);
        
        webSocket("/chat", ChatWebSocketHandler.class);
        init();
    }
    
      
    public synchronized static void broadcastMessage(String message) 
    {
        String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        userUsernameMap.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(new JSONObject()
                    .put("userMessage", "")
                    .put("userlist", userUsernameMap.values())
                    .put("time", timeStamp)
                    .put("from", "")
                    .put("system", message)
                ));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    //Sends a message from one user to all users, along with a list of current usernames    
    // thread safe synchronized for sending message to all users.
    public synchronized static void broadcastMessage(String sender, String message) 
    {
        String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        userUsernameMap.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(new JSONObject()
                    .put("userMessage", message)
                    .put("userlist", userUsernameMap.values())
                    .put("time", timeStamp)
                    .put("from", sender)
                    .put("system", "")
                ));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}