import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

@WebSocket
public class ChatWebSocketHandler {

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        String username = "User" + Chat.nextUserNumber++;
        Chat.userUsernameMap.put(user, username);
        Chat.broadcastMessage("Welcome " +username +".");
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        String username = Chat.userUsernameMap.get(user);
        Chat.userUsernameMap.remove(user);
        Chat.broadcastMessage(username + " left the chat.");
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
    	
    	int lastIndex = message.lastIndexOf("mySecretCombo");
    	int firstIndex = message.indexOf("mySecretCombo");
    	
    	String tempName = message.substring(0, firstIndex).trim();
        if(tempName.length() == 0)
        {
            tempName = "Noname";
        }
    	message = message.substring(lastIndex + 13);
    	
    	Chat.userUsernameMap.put(user, tempName);
        Chat.broadcastMessage(Chat.userUsernameMap.get(user),message);
    }
}